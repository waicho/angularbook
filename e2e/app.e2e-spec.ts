import { AngularBookPage } from './app.po';

describe('angular-book App', () => {
  let page: AngularBookPage;

  beforeEach(() => {
    page = new AngularBookPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
