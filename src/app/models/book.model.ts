export class Book {
    title: string;
    description: string;
    publication_date: Date;
    price: number;
    rating: number;
    in_stock: boolean;
    id: string;
    // author: string;

    constructor(
        title: string,
        description: string,
        publication_date: Date,
        price: number,
        rating: number,
        in_stock: boolean,
        id?: string
        // author: string
    ) {
        this.title = title;
        this.description = description;
        this.publication_date = publication_date;
        this.price = price;
        this.rating = rating;
        this.in_stock = in_stock;
        this.id = id;
        // this.author = author;
    }
}