import { Injectable } from '@angular/core';
import { Book } from '../models/book.model';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { Http,Response,Headers,RequestOptions } from '@angular/http';
import { Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class UpdateService {
  public headers = new Headers({ 'Content-Type' : 'application/json'});
  public options = new RequestOptions({ headers: this.headers });

  constructor(
    public route: Router,
    public router: ActivatedRoute,
    public http: Http
  ) { }

  updateBook(Book: Book) {
    let id: string;
     this.router.queryParams.subscribe(params => {
      if (params['id']) {     
        id = params['id']
      }   
    });   

    console.log(id);
    let url = 'http://192.168.0.112:3000/api/v1/update_book?id=' + id;
    // let bookString = JSON.stringify(Book);

      this.http.put(url, Book, this.options)
        .map((res: Response) => console.log(res.json()))
        .catch(error => Observable.throw(error.json().error) || 'Server Error').subscribe(res => console.log(res));;
      this.route.navigate(['/']);
      window.location.reload();
}
}


  

