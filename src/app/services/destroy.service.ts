import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions} from '@angular/http';



@Injectable()
export class DestroyService {
  public headers = new Headers({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'PUT, GET, POST, DELETE, OPTIONS',
    'Access-Control-Allow-Headers': 'accept, content-type, x-parse-application-id, x-parse-rest-api-key, x-parse-session-token'
  });
  public options = new RequestOptions({ headers: this.headers });

  constructor(
    public http: Http,
    public router: Router
  ) { }

  destroyBook(id) {
    console.log(id);

    let url = 'http://192.168.0.112:3000/api/v1/delete_book?id=' + id;
    this.http.delete(url).subscribe(res=>console.log('delete result',res));
    window.location.reload();
    // this.router.navigate(['/']);
  }

}

