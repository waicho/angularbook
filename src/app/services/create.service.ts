import { Injectable } from '@angular/core';
import { Book } from '../models/book.model';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class CreateService {
  public headers = new Headers({ 'Content-Type': 'application/json' });
  public options = new RequestOptions({ headers: this.headers });

  constructor(
    public router: Router,
    public http: Http
  ) { }

  createBook(Book) {
      console.log(Book);

      let url = 'http://192.168.0.112:3000/api/v1/post_book';
      // let bookString = JSON.stringify(Book);

      this.http.post(url, Book, this.options)
        .map((res: Response) => console.log(res.json()))
        .catch(error => Observable.throw(error.json().error) || 'Server Error').subscribe(res => console.log(res));;
      this.router.navigate(['/']);
      window.location.reload();
  }
}


