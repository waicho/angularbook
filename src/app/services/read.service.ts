import { Injectable } from '@angular/core';
import { Book } from '../models/book.model';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class ReadService {

  public headers = new Headers({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'PUT, GET, POST, DELETE, OPTIONS',
    'Access-Control-Allow-Headers': 'accept, content-type, x-parse-application-id, x-parse-rest-api-key, x-parse-session-token'
  });
  public options = new RequestOptions({ headers: this.headers });

  constructor(
    public http: Http
  ) { }
  getBooks() {
      let url = 'http://192.168.0.112:3000/api/v1/book';
      return this.http.get(url)
        .map(res => res.json())
        .catch(error => Observable.throw(error.json().error || 'Server Error'));
  }

  getBook(id) {
    let url = 'http://192.168.0.112:3000/api/v1/book_show?id='+id;
      return this.http.get(url)
        .map(res => res.json())
        .catch(error => Observable.throw(error.json().error || 'Server Error'));
  }
}
