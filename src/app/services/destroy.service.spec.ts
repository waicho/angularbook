import { TestBed, inject } from '@angular/core/testing';

import { DestroyService } from './destroy.service';

describe('DestroyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DestroyService]
    });
  });

  it('should be created', inject([DestroyService], (service: DestroyService) => {
    expect(service).toBeTruthy();
  }));
});
