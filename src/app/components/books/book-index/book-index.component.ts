import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ReadService } from '../../../services/read.service';
import { DestroyService } from '../../../services/destroy.service';
import { Book } from '../../../models/book.model';


@Component({
  selector: 'app-book-index',
  templateUrl: './book-index.component.html',
  styleUrls: ['./book-index.component.css']
})
export class BookIndexComponent implements OnInit {
  public books: Book[];
  public book: Book;

  constructor(
    public router: Router,
    public readService: ReadService,
    public destroyService: DestroyService
  ) {}

  ngOnInit() {
    this.readService.getBooks().subscribe(res=>{
      console.log(res);
      this.books = res;
    })
    console.log(this.books);
    
  }

  editBook(id) {
    this.router.navigate(['/books/form'], {
      queryParams:
      {
        id: id
      }
    })
  }

  destroyBook(id) {
    this.destroyService.destroyBook(id);
  }
}
