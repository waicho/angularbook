import { Component, OnInit } from '@angular/core';
import { 
  ReactiveFormsModule,
  FormsModule,
  FormGroup,
  FormControl,
  Validators,
  FormBuilder
} from '@angular/forms';
import { Book } from '../../../models/book.model';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { CreateService } from '../../../services/create.service';
import { ReadService } from '../../../services/read.service';
import { UpdateService } from '../../../services/update.service';


@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.css']
})
export class BookFormComponent implements OnInit {
  public title: FormControl;
  public description: FormControl;
  public publication_date: FormControl;
  public price: FormControl;
  public rating: FormControl;
  public in_stock: FormControl;

  public addBookForm: FormGroup;
  public book: Book;

  constructor(
    public route: ActivatedRoute,
    public formBuilder: FormBuilder,
    public createService: CreateService,
    public updateService: UpdateService,
    public readService: ReadService
  ) { }

  ngOnInit() {
    //Form Validation
    this.title = new FormControl('', [
      Validators.required
    ]);
    this.description = new FormControl('', [
      Validators.required
    ]);
    this.publication_date = new FormControl('', [
      Validators.required
    ]);
    this.price = new FormControl('', [
      Validators.required
    ]);
    this.rating = new FormControl('', [
      Validators.required
    ]);
    this.in_stock = new FormControl('');

    this.route.queryParams.subscribe(params => {
      if (params['id']) {
       this.readService.getBook(params['id']).subscribe(res=>{ 
         this.book = res
          this.title.setValue(this.book.title);
          this.description.setValue(this.book.description);
          this.publication_date.setValue(this.book.publication_date);
          this.price.setValue(this.book.price);
          this.rating.setValue(this.book.rating);
          this.in_stock.setValue(this.book.in_stock);
        });
        
      }
      else
        this.book = null;
    }); 

    this.buildForm();
  }
  buildForm(): void {
    this.addBookForm = this.formBuilder.group({
      title: this.title,
      description: this.description,
      publication_date: this.publication_date,
      price: this.price,
      rating: this.rating,
      in_stock: this.in_stock
    });
  }
  onSubmit() {
    let book: Book;

      book = new Book(
        this.title.value,
        this.description.value,
        new Date(this.publication_date.value),
        this.price.value,
        this.rating.value,
        this.in_stock.value);

    if (this.book)
      this.updateService.updateBook(book);
    else
      this.createService.createBook(book);
  }
}

    