import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ReadService } from '../../../services/read.service';
import { Book } from '../../../models/book.model';


@Component({
  selector: 'app-book-show',
  templateUrl: './book-show.component.html',
  styleUrls: ['./book-show.component.css']
})
export class BookShowComponent implements OnInit {
  public book: Book;

  constructor(
    public route: ActivatedRoute,
    public readService: ReadService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params =>{

      this.readService.getBook(params['id']).subscribe(res=>{
        console.log(res);
        this.book = res;
      })
      // console.log(this.book);
  
    });
  }
}
