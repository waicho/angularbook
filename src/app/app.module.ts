import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { BookFormComponent } from './components/books/book-form/book-form.component';
import { BookIndexComponent } from './components/books/book-index/book-index.component';
import { BookShowComponent } from './components/books/book-show/book-show.component';
import { MainPageComponent } from './components/main-page/main-page.component';

import { CreateService } from './services/create.service';
import { ReadService } from './services/read.service';
import { UpdateService } from './services/update.service';
import { DestroyService } from './services/destroy.service';


const appRoutes: Routes = [
  { path: '', component: MainPageComponent },
  { path: 'books/form', component: BookFormComponent },
  { path: 'books/:id', component: BookShowComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    BookIndexComponent,
    MainPageComponent,
    BookShowComponent,
    BookFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [CreateService, ReadService, UpdateService, DestroyService],
  bootstrap: [AppComponent]
})
export class AppModule { }
